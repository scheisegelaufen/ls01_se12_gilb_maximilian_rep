import java.util.Scanner;

public class KonsoleneingabeZusatzAufgaben {

	public static void main(String[] args) {
		
		 Scanner einlesen = new Scanner (System.in);
		 
		 double zahl1 = 0, zahl2 = 0, ergebnisAdd = 0, ergebnisSub = 0, ergebnisMult = 0, ergebnisDiv = 0;
		 
		 System.out.print("Bitte geben Sie eine ganze Zahl ein : ");
		 zahl1 = einlesen.nextDouble();
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 zahl2 = einlesen.nextDouble();
		 
		 ergebnisAdd = zahl1 + zahl2;
		 ergebnisSub = zahl1 - zahl2;
		 ergebnisMult = zahl1 * zahl2;
		 ergebnisDiv = zahl1 / zahl2;
		 
		 System.out.println("\nAddition");
		 System.out.println("   " + zahl1 + " + " + zahl2 + " = " + ergebnisAdd);
		 System.out.println("Subtraktion");
		 System.out.println("   " + zahl1 + " - " + zahl2 + " = " + ergebnisSub);
		 System.out.println("Multiplikation");
		 System.out.println("   " + zahl1 + " * " + zahl2 + " = " + ergebnisMult);
		 System.out.println("Division");
		 System.out.println("   " + zahl1 + " / " + zahl2 + " = " + ergebnisDiv);
		 

	}

}
