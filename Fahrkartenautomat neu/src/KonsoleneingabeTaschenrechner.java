import java.util.Scanner; // Import der Klasse Scanner

public class KonsoleneingabeTaschenrechner {

	public static void main(String[] args) {
		
		// Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie eine Zahl ein: ");

		 double zahl1 = myScanner.nextDouble();

		 System.out.print("Bitte geben Sie eine zweite Zahl ein: ");

		 double zahl2 = myScanner.nextDouble();

		 double ergebnisAdd = zahl1 + zahl2;
		 double ergebnisSub = zahl1 - zahl2;
		 double ergebnisMult = zahl1 * zahl2;
		 double ergebnisDiv = zahl1 / zahl2;

		 System.out.print("\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAdd);
		 
		 System.out.print("\n\nErgebnis der Subtraktion lautet: ");
		 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSub);
		 
		 System.out.print("\n\nErgebnis der Multiplikation lautet: ");
		 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMult);
		 
		 System.out.print("\n\nErgebnis der Division lautet: ");
		 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDiv);
		 
		 myScanner.close();
		 
		 //stage-Kommnentar, bitte ignorieren
		 
	}

}
